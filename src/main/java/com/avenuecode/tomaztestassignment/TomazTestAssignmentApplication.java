package com.avenuecode.tomaztestassignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TomazTestAssignmentApplication {
	private static Logger logger = LoggerFactory.getLogger(TomazTestAssignmentApplication.class);

	public static void main(String[] args) {
		logger.info("Application started.");
		SpringApplication.run(TomazTestAssignmentApplication.class, args);
	}
}
