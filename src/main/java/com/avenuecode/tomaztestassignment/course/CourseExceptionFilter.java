package com.avenuecode.tomaztestassignment.course;

import com.avenuecode.tomaztestassignment.course.errors.CourseNotFoundException;
import com.avenuecode.tomaztestassignment.shared.dtos.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class CourseExceptionFilter {
    private static Logger logger = LoggerFactory.getLogger(CourseController.class);

    @ExceptionHandler(CourseNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse handleCourseNotFound (HttpServletRequest req, CourseNotFoundException ex) {
        logger.warn("Client requested an unregistered course. Requested Id: {}", ex.getRequestedId());

        return new ErrorResponse(ex.getMessage());
    }
}
