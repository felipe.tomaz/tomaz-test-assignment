package com.avenuecode.tomaztestassignment.course;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.List;

@Entity
public class Course {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String institutionName;
    private Calendar startDate;
    private Calendar endDate;

    public Course () {}

    public Course(String name, String institutionName, Calendar startDate, Calendar endDate) {
        this.name = name;
        this.institutionName = institutionName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() { return id; }

    public String getName() {
        return name;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public long getCourseDurationInHours() {
        LocalDate start = LocalDate.ofInstant(startDate.toInstant(), ZoneId.systemDefault());
        LocalDate end = LocalDate.ofInstant(endDate.toInstant(), ZoneId.systemDefault());
        List<DayOfWeek> weekend = List.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
        int numberOfHoursPerDay = 8;

        long numberOfDaysInThePeriod = start.datesUntil(end.plusDays(1))
                .filter((date) -> !weekend.contains(date.getDayOfWeek()))
                .count();
        return numberOfDaysInThePeriod * numberOfHoursPerDay;
    }
}
