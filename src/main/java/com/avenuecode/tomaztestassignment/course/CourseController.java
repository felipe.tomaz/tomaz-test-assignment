package com.avenuecode.tomaztestassignment.course;

import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseRequest;
import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseResponse;
import com.avenuecode.tomaztestassignment.course.dtos.GetCourseResponse;
import com.avenuecode.tomaztestassignment.course.errors.CourseNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/courses")
public class CourseController {
    @Autowired
    private CourseService courseService;

    private Logger logger;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
        logger = LoggerFactory.getLogger(CourseController.class);
    }

    @PostMapping("/")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCourseResponse createCourse(@Valid @RequestBody CreateCourseRequest request) {
        Course created = courseService.createCourse(request);
        logger.info("Course " + created.getName() + " created successfully.");

        return new CreateCourseResponse(created.getId(), created.getCourseDurationInHours());
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public GetCourseResponse getCourseById(@PathVariable("id") Long id) throws Exception {
        Course course = courseService.getCourseById(id);

        return new GetCourseResponse(course);
    }
}
