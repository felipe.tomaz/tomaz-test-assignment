package com.avenuecode.tomaztestassignment.course.errors;

public class InvalidPeriodException extends RuntimeException {
    public InvalidPeriodException() {
        super("The informed period is invalid.");
    }
}
