package com.avenuecode.tomaztestassignment.course.errors;

public class CourseNotFoundException extends RuntimeException {
    private Long requestedId;

    public CourseNotFoundException (Long id) {
        super(String.format("Course with id %d not found.", id));
        this.requestedId = id;
    }

    public Long getRequestedId() {
        return requestedId;
    }
}
