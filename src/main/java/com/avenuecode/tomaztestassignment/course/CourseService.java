package com.avenuecode.tomaztestassignment.course;

import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseRequest;
import com.avenuecode.tomaztestassignment.course.errors.CourseNotFoundException;
import com.avenuecode.tomaztestassignment.course.errors.InvalidPeriodException;
import com.avenuecode.tomaztestassignment.utils.NameFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;

@Service
public class CourseService {
    @Autowired
    private NameFormatter formatter;
    @Autowired
    private CourseRepository repository;

    public CourseService(NameFormatter formatter, CourseRepository repository) {
        this.formatter = formatter;
        this.repository = repository;
    }

    public Course createCourse (CreateCourseRequest request) {
        Calendar start = request.getStartDate();
        Calendar end = request.getEndDate();
        boolean isPeriodInvalid = end.before(start);

        if(isPeriodInvalid) {
            throw new InvalidPeriodException();
        }

        String name = formatter.format(request.getName());
        Course course = new Course(
                name,
                request.getInstitutionName(),
                request.getStartDate(),
                request.getEndDate()
        );

        return repository.save(course);
    }

    public Course getCourseById(Long id) {
        Optional<Course> course = this.repository.findById(id);
        if (course.isEmpty()) {
            throw new CourseNotFoundException(id);
        }

        return course.get();
    }
}
