package com.avenuecode.tomaztestassignment.course.dtos;

public class CreateCourseResponse {
    private Long courseId;
    private Long courseDurationInHours;

    public CreateCourseResponse(Long courseId, Long courseDurationInHours) {
        this.courseId = courseId;
        this.courseDurationInHours = courseDurationInHours;
    }

    public Long getCourseId() {
        return courseId;
    }

    public Long getCourseDurationInHours() {
        return courseDurationInHours;
    }
}
