package com.avenuecode.tomaztestassignment.course.dtos;

import com.avenuecode.tomaztestassignment.course.Course;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Locale;

public class GetCourseResponse {
    private Long id;
    private String name;
    private String institution;
    private Long totalWorkload;
    private String startDate;
    private String endDate;

    public GetCourseResponse(Course course) {
        this.id = course.getId();
        this.name = course.getName();
        this.institution = course.getInstitutionName();
        this.totalWorkload = course.getCourseDurationInHours();
        this.startDate = getDateString(course.getStartDate());
        this.endDate = getDateString(course.getEndDate());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInstitution() {
        return institution;
    }

    public Long getTotalWorkload() {
        return totalWorkload;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    private String getDateString (Calendar date) {
        LocalDate localDate = LocalDate.ofInstant(date.toInstant(), ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter
                .ofLocalizedDate(FormatStyle.LONG)
                .localizedBy(Locale.ENGLISH);

        return localDate.format(formatter);
    }


}
