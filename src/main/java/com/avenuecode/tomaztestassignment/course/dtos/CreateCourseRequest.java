package com.avenuecode.tomaztestassignment.course.dtos;

import java.util.Calendar;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateCourseRequest {
    @NotNull(message = "The course name must be defined.")
    @NotBlank(message = "The course name must be not empty.")
    private String name;

    @NotNull(message = "The institutionName param must be defined.")
    @NotBlank(message = "The institutionName param must be not empty.")
    private String institutionName;

    @NotNull (message = "The startDate param must be defined. Pattern yyyy-MM-dd")
    private Calendar startDate;

    @NotNull (message = "The endDate param must be defined. Pattern yyyy-MM-dd")
    private Calendar endDate;

    public CreateCourseRequest(String name, String institutionName, Calendar startDate, Calendar endDate) {
        this.name = name;
        this.institutionName = institutionName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }
}
