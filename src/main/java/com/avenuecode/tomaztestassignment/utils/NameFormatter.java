package com.avenuecode.tomaztestassignment.utils;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class NameFormatter {
    public String format(String name) {
        String[] parts = name.trim().split("\\s+");
        List<String> capitalized= Arrays.stream(parts)
                .map(this::capitalize)
                .collect(Collectors.toList());

        return String.join(" ", capitalized);
    }

    private String capitalize (String word) {
        String firstLetter = word.substring(0, 1).toUpperCase();
        String rest = word.substring(1).toLowerCase();

        return firstLetter.concat(rest);
    }
}
