package com.avenuecode.tomaztestassignment.shared;

import com.avenuecode.tomaztestassignment.shared.dtos.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ApplicationExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public List<ErrorResponse> sendInvalidArgumentErrorResponse (HttpServletRequest req, MethodArgumentNotValidException ex) {
        logger.info("Request with invalid params.", ex);

        return ex.getAllErrors()
                .stream()
                .map((e) -> new ErrorResponse(e.getDefaultMessage()))
                .collect(Collectors.toList());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse sendInternalServerErrorResponse (HttpServletRequest req, Exception ex) throws IOException {
        Map<String, String> request = new HashMap();
        request.put("URI", req.getRequestURI());
        request.put("Method", req.getMethod());
        request.put("Body", req.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
        req.getHeaderNames().asIterator().forEachRemaining((header) -> {
            request.put(header, req.getHeader(header));
        });

        logger.error("Occurred an internal server error when processing the request {}:", request);
        logger.error("Thrown exception: ", ex);

        return new ErrorResponse("Sorry, occurred an error while processing your request. Try again later.");
    }
}
