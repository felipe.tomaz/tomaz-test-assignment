package com.avenuecode.tomaztestassignment.acceptance;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CreateCourseSteps {

    @LocalServerPort
    private int port;
    private Map<String, String> body = new HashMap<>();
    private ValidatableResponse response;

    @Given("^the course name is (.+)$")
    public void the_course_name_is(String courseName) {
        addBodyParam("name", courseName);
    }

    @Given("the institution name is (.+)$")
    public void the_institution_name_is(String institutionName) {
        addBodyParam("institutionName", institutionName);
    }

    @Given("the start date is (.+)$")
    public void the_start_date_is(String startDate) {
        addBodyParam("startDate", startDate);
    }

    @Given("the end date is (.+)$")
    public void the_end_date_is(String endDate) {
        addBodyParam("endDate", endDate);
    }

    @When("the client make a POST request to \\/courses")
    public void the_client_make_a_post_request_to_courses() {
        this.response = given()
                .port(port)
                .contentType(ContentType.JSON)
                .body(new JSONObject(body).toString())
                .when()
                .post("/courses/")
                .then();
    }

    @Then("the client receives a status code of {int}")
    public void the_client_receives_a_status_code_of(Integer statusCode) {
        response.statusCode(statusCode);
    }

    @Then("a response body containing the id of the new course")
    public void a_response_body_containing_the_id_of_the_new_course() {
        response.body("courseId", Matchers.notNullValue());
    }

    @Then("a total course duration in hours of {int}.")
    public void a_total_course_duration_in_hours_of(Integer workload) {
        response.body("courseDurationInHours", equalTo(workload));
    }

    public void addBodyParam(String key, String value) {
        this.body.put(key, value);
    }
}
