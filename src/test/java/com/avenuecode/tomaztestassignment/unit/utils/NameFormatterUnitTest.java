package com.avenuecode.tomaztestassignment.unit.utils;

import com.avenuecode.tomaztestassignment.utils.NameFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NameFormatterUnitTest {
    NameFormatter sut = new NameFormatter();

    @Test
    public void shouldCapitalizeTheName() {
        String formatted = sut.format("sOME NAme");

        assertEquals("Some Name", formatted);
    }

    @Test
    public void shouldRemoveExtraSpaces () {
        String expected = "Some Name";

        assertEquals(expected, sut.format("Some       Name"));
        assertEquals(expected, sut.format("     Some    Name"));
        assertEquals(expected, sut.format("Some      Name      "));
        assertEquals(expected, sut.format("Some Name      "));
    }
}
