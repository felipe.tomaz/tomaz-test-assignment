package com.avenuecode.tomaztestassignment.unit.course;

import com.avenuecode.tomaztestassignment.course.Course;
import com.avenuecode.tomaztestassignment.course.CourseRepository;
import com.avenuecode.tomaztestassignment.course.CourseService;
import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseRequest;
import com.avenuecode.tomaztestassignment.course.errors.InvalidPeriodException;
import com.avenuecode.tomaztestassignment.utils.NameFormatter;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public class CourseServiceUnitTest {
    private CourseRepository repository;
    private NameFormatter formatter;
    private CourseService sut;
    private Faker faker;

    @BeforeEach
    public void init() {
        repository = Mockito.mock(CourseRepository.class);
        formatter = Mockito.mock(NameFormatter.class);
        sut = new CourseService(formatter, repository);
        faker = new Faker();
    }

    @Test
    public void shouldReturnTheNewCourseRecord() {
        CreateCourseRequest request = new CreateCourseRequest(
            faker.educator().course(),
            faker.educator().university(),
            new GregorianCalendar(),
            new GregorianCalendar()
        );

        Course course = mock(Course.class);
        when(repository.save(any(Course.class))).thenReturn(course);
        Course returned = sut.createCourse(request);

        assertEquals(course, returned);
    }

    @Test
    public void shouldFormatTheCourseName() {
        CreateCourseRequest request = new CreateCourseRequest(
                faker.educator().course(),
                faker.educator().university(),
                new GregorianCalendar(),
                new GregorianCalendar()
        );

        sut.createCourse(request);

        verify(formatter, times(1)).format(request.getName());
    }

    @Test
    public void shouldValidateCoursePeriod() {
        Calendar start = new GregorianCalendar(2021, Calendar.MARCH, 1);
        Calendar end = new GregorianCalendar(2021, Calendar.FEBRUARY, 1);
        CreateCourseRequest request = new CreateCourseRequest(
                faker.educator().course(),
                faker.educator().university(),
                start,
                end
        );

        assertThrows(InvalidPeriodException.class, () -> sut.createCourse(request));
    }
}
