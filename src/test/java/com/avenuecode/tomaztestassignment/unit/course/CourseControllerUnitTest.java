package com.avenuecode.tomaztestassignment.unit.course;

import com.avenuecode.tomaztestassignment.course.Course;
import com.avenuecode.tomaztestassignment.course.CourseController;
import com.avenuecode.tomaztestassignment.course.CourseService;
import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseRequest;
import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseResponse;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CourseControllerUnitTest {

    @Test
    public void shouldReturnTheCreationResponseWithTheWorkload() {
        Faker faker = new Faker();
        CourseService service = mock(CourseService.class);
        CourseController sut = new CourseController(service);
        CreateCourseRequest request = new CreateCourseRequest(
                faker.educator().course(),
                faker.educator().university(),
                new GregorianCalendar(),
                new GregorianCalendar()
        );
        Course course = mock(Course.class);
        Long expectedId = 50L;
        Long expectedWorkload = 1000L;
        when(course.getId()).thenReturn(expectedId);
        when(course.getCourseDurationInHours()).thenReturn(expectedWorkload);
        when(service.createCourse(request)).thenReturn(course);

        CreateCourseResponse response = sut.createCourse(request);

        assertEquals(expectedId, response.getCourseId());
        assertEquals(expectedWorkload, response.getCourseDurationInHours());
    }
}
