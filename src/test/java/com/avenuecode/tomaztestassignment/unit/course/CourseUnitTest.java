package com.avenuecode.tomaztestassignment.unit.course;

import com.avenuecode.tomaztestassignment.course.Course;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CourseUnitTest {
    private final Faker faker = new Faker();

    @Test
    public void shouldCalculateCourseDurationInHours () {

        Course course = new Course(
                faker.educator().course(),
                faker.educator().university(),
                new GregorianCalendar(2021, Calendar.SEPTEMBER, 15),
                new GregorianCalendar(2021, Calendar.DECEMBER, 6)
        );

        assertEquals(472, course.getCourseDurationInHours());
    }
}
