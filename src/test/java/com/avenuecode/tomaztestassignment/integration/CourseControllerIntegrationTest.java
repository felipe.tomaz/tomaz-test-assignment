package com.avenuecode.tomaztestassignment.integration;

import com.avenuecode.tomaztestassignment.course.Course;
import com.avenuecode.tomaztestassignment.course.CourseService;
import com.avenuecode.tomaztestassignment.course.dtos.CreateCourseRequest;
import com.github.javafaker.Faker;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.GregorianCalendar;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class CourseControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private Faker faker;

    @MockBean
    private CourseService courseService;

    @BeforeEach
    private void init () {
        faker = new Faker();
        Course course = new Course(
                faker.educator().course(),
                faker.educator().university(),
                new GregorianCalendar(),
                new GregorianCalendar()
        );

        course.setId(500L);

        when(courseService.createCourse(any(CreateCourseRequest.class)))
                .thenReturn(course);
    }

    @Test
    public void shouldNotAcceptAnEmptyName() throws Exception {
        JSONObject content = getRequestExample();
        content.put("name", "");

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldNotAcceptAnEmptyInstitutionName() throws Exception {
        JSONObject content = getRequestExample();
        content.put("institutionName", "");

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldNotAcceptAnUndefinedPeriod() throws Exception {
        JSONObject content1 = getRequestExample();
        JSONObject content2 = getRequestExample();
        content1.put("startDate", "");
        content2.put("endDate", "");

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content1.toString())
        ).andExpect(status().isBadRequest());

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content2.toString())
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldValidateTheDateFormat() throws Exception {
        JSONObject content1 = getRequestExample();
        JSONObject content2 = getRequestExample();
        content1.put("startDate", "21-02-202");
        content2.put("endDate", "21-06-202");

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content1.toString())
        ).andExpect(status().isBadRequest());

        mockMvc.perform(post("/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content2.toString())
        ).andExpect(status().isBadRequest());
    }

    private JSONObject getRequestExample() throws JSONException {
        JSONObject content = new JSONObject();
        content.put("name", faker.educator().course());
        content.put("institutionName", faker.educator().university());
        content.put("startDate", "2021-01-02");
        content.put("endDate", "2021-04-02");

        return content;
    }
}
