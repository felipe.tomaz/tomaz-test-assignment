Feature: Create courses
  Scenario: client makes a call to POST /courses
    Given the course name is ACP2P
    And the institution name is Avenue Code
    And the start date is 2021-07-26
    And the end date is 2021-09-26
    When the client make a POST request to /courses
    Then the client receives a status code of 201
    And a response body containing the id of the new course
    And a total course duration in hours of 360.